// express config
const express = require('express');
const server = express();
const async = require('async');

// DB Config
const mongoose = require('mongoose');
const dbURI = process.env.MONGO_URI? process.env.MONGO_URI : 'mongodb://localhost:27017/fixed_task';
 
mongoose.connect(dbURI,{ useNewUrlParser: true });
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open',  () => {
  console.log("Connected to database successfully");
});
const cardModel = require('./models/cards');

server.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

// list api
server.get('/list',(req, res) => {
  
  if(req.query.pageNo){
    
    cardModel.find({}).skip((parseInt(req.query.pageNo) - 1)*8).limit(8).exec((err,cards)=>{
      if(err){
        res.json({
          err : 'error with the database'
        });
      } else {
        res.json({
          cards
        });
      }
    });

  } else {

    async.series([
      function count(cb) {
        cardModel.estimatedDocumentCount((err,count)=>{
          return cb(null,count);
        })
      },
      function list(cb) { 
        cardModel.find({}).limit(8).exec((err,cards)=>{
          if(err){
            return cb('error with the database');
          } else {
            return cb(null,cards);
          }
        });
      }
    ],(err,results)=>{
      if(err){
        res.status(400).json({
          err
        });
      } else {
        res.status(200).json({
          cards : results[1],
          count : results[0]
        });
      }
    });

  }

});


server.get('/search',(req, res) => {

  if(req.query.pageNo){

    cardModel.find({name: { "$regex": req.query.str, "$options": "i" }}).skip((parseInt(req.query.pageNo) - 1)*8).limit(8).exec((err,cards)=>{
      if(err){
        res.json({
          err : 'error with the database'
        });
      } else {
        res.json({
          cards
        });
      }
    });

  } else {
  
    async.series([
      function count(cb) {
        cardModel.countDocuments({name: { "$regex": req.query.str, "$options": "i" }},(err,count)=>{
          return cb(null,count);
        })
      },
      function list(cb) { 
        cardModel.find({name: { "$regex": req.query.str, "$options": "i" }}).limit(8).exec((err,cards)=>{
          if(err){
            return cb('error with the database');
          } else {
            return cb(null,cards);
          }
        });
      }
    ],(err,results)=>{
      if(err){
        res.status(400).json({
          err
        });
      } else {
        res.status(200).json({
          cards : results[1],
          count : results[0]
        });
      }
    });

  }

});


var port = 9000;
server.listen(port,()=>{
    console.log("server  on");
});