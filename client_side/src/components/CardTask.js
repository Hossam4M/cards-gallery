import React from 'react';
import { Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle } from 'reactstrap';

export default class CardTask extends React.Component {

  render(){
    let features = JSON.parse(this.props.features)

    return (
        <Card>
          <CardImg top width="100%" height="250px" src={features.image} alt="Card image cap" />
          <CardBody>
            <CardTitle>{features.name}</CardTitle>
            <CardSubtitle>{features.amiiboSeries}</CardSubtitle>
            <CardText>Tail : {features.tail}</CardText>
          </CardBody>
        </Card>
    );
  }
};
