import React from 'react';
import { InputGroup, InputGroupAddon, Input } from 'reactstrap';

export default class SearchBar extends React.Component {

  search = (e) => {
    this.props.onSearch(e.target.value);
  }

  render(){

    return (

      <InputGroup>
        <InputGroupAddon addonType="prepend">Search</InputGroupAddon>
        <Input placeholder="Type what you want :)" onChange={this.search}/>
      </InputGroup>

    );

  }

}