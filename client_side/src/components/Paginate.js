import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

export default class Paginate extends React.Component {
  
  handleClick = (e) => {
    this.props.onNavigation(e.target.text);
  }

  handleClickNext = (e) => {
    e.preventDefault();
    this.props.onNavigationNext(e);
  }

  handleClickPrev = (e) => {
    e.preventDefault();
    this.props.onNavigationPrev(e);
  }

  render(){

    let pages = [];
    if (this.props.end === 1) {
      pages.push(
        <PaginationItem key='1'>
          <PaginationLink href="#" onClick={this.handleClick}>
            There is no items for your search
          </PaginationLink>
        </PaginationItem>
      );
    } else {
      for (let i = this.props.start; i < this.props.end; i++) {
        pages.push(
          <PaginationItem key={i}>
            <PaginationLink href="#" onClick={this.handleClick}>
              {i}
            </PaginationLink>
          </PaginationItem>
        );
      }
    }
    

    return(
      <Pagination aria-label="Page navigation example">
        <PaginationItem>
          <PaginationLink previous href="#" onClick={this.handleClickPrev}/>
        </PaginationItem>

        {pages}
        
        <PaginationItem>
          <PaginationLink next href="#" onClick={this.handleClickNext}/>
        </PaginationItem>
      </Pagination>
    );

  }

}